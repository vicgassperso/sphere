<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Résultats des Sphères</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        h2 {
            text-align: center;
            color: #333;
        }

        table {
            border-collapse: collapse;
            width: 80%;
            margin: 20px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            background-color: #fff;
            border-radius: 8px;
            overflow: hidden;
        }

        th,
        td {
            border: 1px solid #ddd;
            text-align: center;
            padding: 12px;
        }

        th {
            background-color: #f2f2f2;
            color: #333;
        }

        tr:nth-child(even) {
            background-color: #f9f9f9;
        }

        tr:hover {
            background-color: #e0e0e0;
        }

        p {
            text-align: center;
            color: #555;
        }
    </style>
</head>

<body>

    <h2>Résultats des Sphères</h2>

    <?php
    $nom_fichier = "resultats_spheres.csv";

    if (file_exists($nom_fichier)) {
        $donnees = array_map('str_getcsv', file($nom_fichier));

        echo "<table>";
        echo "<tr><th>Rayon</th><th>Volume</th></tr>";

        foreach ($donnees as $ligne) {
            echo "<tr>";
            foreach ($ligne as $valeur) {
                echo "<td>{$valeur}</td>";
            }
            echo "</tr>";
        }

        echo "</table>";
    } else {
        echo "<p>Aucun résultat disponible pour le moment.</p>";
    }
    ?>

</body>

</html>
