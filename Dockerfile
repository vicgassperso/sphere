# Utilisez une image de base Python
FROM python:3.9

# Définissez le répertoire de travail dans le conteneur
WORKDIR /app

# Copiez le code source dans le conteneur
COPY . /app

# Installez les dépendances Python
RUN pip install --no-cache-dir -r requirements.txt

# Exposez le port sur lequel votre application s'exécute
EXPOSE 8080

# Commande pour exécuter votre application
CMD ["python", "sphere.py"]
