import unittest
import math
from sphere import calculer_volume_sphere, enregistrer_volume_sphere

class TestCalculVolumeSphere(unittest.TestCase):
    def test_calculer_volume_sphere_rayon_nul(self):
        self.assertEqual(calculer_volume_sphere(0), 0)

    def test_calculer_volume_sphere_rayon_unite(self):
        self.assertAlmostEqual(calculer_volume_sphere(1), (4/3) * math.pi, delta=0.0001)

    def test_calculer_volume_sphere_rayon_2_5(self):
        self.assertAlmostEqual(calculer_volume_sphere(2.5), 65.45, delta=0.01)

    def test_calculer_volume_sphere_rayon_negatif(self):
        self.assertIsNone(calculer_volume_sphere(-1))

    def test_calculer_volume_sphere_rayon_non_numerique(self):
        with self.assertRaises(TypeError):
            calculer_volume_sphere("abc")

if __name__ == '__main__':
    unittest.main()
