"""
Module pour le calcul du volume d'une sphère et l'enregistrement des résultats dans un fichier CSV.
"""

import csv
import math

def calculer_volume_sphere(rayon):
    """
    Calcule le volume d'une sphère en fonction de son rayon.
    Args:
        rayon (float): Le rayon de la sphère.
    Returns:
        float: Le volume de la sphère.
    """
    if not isinstance(rayon, (int, float)):
        raise TypeError("Le rayon doit être un nombre.")
    if rayon < 0:
        return None
    volume = (4/3) * math.pi * (rayon**3)
    return volume

def enregistrer_volume_sphere(rayon, nom_fichier):
    """
    Enregistre le volume d'une sphère dans un fichier CSV.
    Args:
        rayon (float): Le rayon de la sphère.
        nom_fichier (str): Le nom du fichier CSV.
    """
    volume_sphere = calculer_volume_sphere(rayon)
    with open(nom_fichier, mode='a', newline='', encoding='utf-8') as fichier_csv:
        writer = csv.writer(fichier_csv)
        if fichier_csv.tell() == 0:
            writer.writerow(["Rayon", "Volume"])
        writer.writerow([rayon, volume_sphere])
    if volume_sphere is not None:
        print(f"Sphère rayon de {rayon} enregistré dans {nom_fichier}.")
    else:
        print("Le rayon doit être un nombre positif pour calculer le volume de la sphère.")

if __name__ == '__main__':
    try:
        rayonsphere = float(input("Entrez le rayon de la sphère : "))
        NOM_FICHIER = "resultats_spheres.csv"
        enregistrer_volume_sphere(rayonsphere, NOM_FICHIER)
    except ValueError:
        print("Veuillez entrer un nombre valide pour le rayon de la sphère.")
 